# Draw On Your Screen

Start drawing with Super+Alt+D.
Then save your beautiful work by taking a screenshot.

![](https://framagit.org/abakkk/DrawOnYourScreen/raw/ressources/screenshot.jpg)

## Features

* Basic shapes (rectangle, circle, ellipse, line, curve, text, free)
* Smooth stroke
* Draw over applications
* Keep drawings on desktop background with persistence (notes, children's art ...)
* Multi-monitor support
* Export to SVG

## Install

1. Download and decompress or clone the repository
2. Place the resulting directory in `~/.local/share/gnome-shell/extensions`
3. **Change the directory name** to `drawOnYourScreen@abakkk.framagit.org`
4. Xorg: type `alt + F2` and `r` to restart gnome-shell  
   Wayland: restart or re-login
5. Enable the extension in gnome-tweaks
6. `Super + Alt + D` to test
7. [https://framagit.org/abakkk/DrawOnYourScreen/issues](https://framagit.org/abakkk/DrawOnYourScreen/issues) to say it doesn't work

## Details

* Draw arrows:

 Intersect two lines and curve the second thanks to the `Ctrl` key.

 ![How to draw an arrow](https://framagit.org/abakkk/DrawOnYourScreen/uploads/af8f96d33cfeff49bb922a1ef9f4a4ce/arrow-screencast.webm)

* Screenshot Tool extension:

 [Screenshot Tool](https://extensions.gnome.org/extension/1112/screenshot-tool/) is a convenient extension to “create, copy, store and upload screenshots”. To use it while drawing mode is active, toggle the area selection mode thanks to the Screenshot Tool shortcut (`Super + F11` by default, see its preferences) and **hold** the `space` key when selecting the area with pointer to avoid drawing.

